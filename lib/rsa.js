'use strict';

var RSA = module.exports;
var SSH = require('./ssh.js');
var PEM = require('./pem.js');
var x509 = require('./x509.js');
var ASN1 = require('./asn1.js');
var Enc = require('./encoding.js');
var Crypto = require('./crypto.js');

/*global Promise*/
RSA.generate = function (opts) {
  opts.kty = "RSA";
  return Crypto.generate(opts).then(function (pair) {
    var format = opts.format;
    var encoding = opts.encoding;

    // The easy way
    if ('json' === format && !encoding) {
      format = 'jwk';
      encoding = 'json';
    }
    if (('jwk' === format || !format) && ('json' === encoding || !encoding)) { return pair; }
    if ('jwk' === format || 'json' === encoding) {
      throw new Error("format '" + format + "' is incompatible with encoding '" + encoding + "'");
    }

    // The... less easy way
    /*
    var priv;
    var pub;

    if ('spki' === format || 'pkcs8' === format) {
      format = 'pkcs8';
      pub = 'spki';
    }

    if ('pem' === format) {
      format = 'pkcs1';
      encoding = 'pem';
    } else if ('der' === format) {
      format = 'pkcs1';
      encoding = 'der';
    }

    priv = format;
    pub = pub || format;

    if (!encoding) {
      encoding = 'pem';
    }

    if (priv) {
      priv = { type: priv, format: encoding };
      pub = { type: pub, format: encoding };
    } else {
      // jwk
      priv = { type: 'pkcs1', format: 'pem' };
      pub = { type: 'pkcs1', format: 'pem' };
    }
    */
    if (('pem' === format || 'der' === format) && !encoding) {
      encoding = format;
      format = 'pkcs1';
    }

    var exOpts = { jwk: pair.private, format: format, encoding: encoding };
    return RSA.export(exOpts).then(function (priv) {
      exOpts.public = true;
      if ('pkcs8' === exOpts.format) { exOpts.format = 'spki'; }
      return RSA.export(exOpts).then(function (pub) {
        return { private: priv, public: pub };
      });
    });
  });
};

RSA.importSync = function (opts) {
  if (!opts || !opts.pem || 'string' !== typeof opts.pem) {
    throw new Error("must pass { pem: pem } as a string");
  }

  var jwk = { kty: 'RSA', n: null, e: null };
  if (0 === opts.pem.indexOf('ssh-rsa ')) {
    return SSH.parse(opts.pem, jwk);
  }
  var pem = opts.pem;
  var block = PEM.parseBlock(pem);
  //var hex = toHex(u8);
  var asn1 = ASN1.parse(block.bytes);

  var meta = x509.guess(block.bytes, asn1);

  if ('pkcs1' === meta.format) {
    jwk = x509.parsePkcs1(block.bytes, asn1, jwk);
  } else {
    jwk = x509.parsePkcs8(block.bytes, asn1, jwk);
  }

  if (opts.public) {
    jwk = RSA.neuter(jwk);
  }
  return jwk;
};
RSA.parse = function parseRsa(opts) {
  // wrapped in a promise for API compatibility
  // with the forthcoming browser version
  // (and potential future native node capability)
  return Promise.resolve().then(function () {
    return RSA.importSync(opts);
  });
};
RSA.toJwk = RSA.import = RSA.parse;

/*
RSAPrivateKey ::= SEQUENCE {
  version           Version,
  modulus           INTEGER,  -- n
  publicExponent    INTEGER,  -- e
  privateExponent   INTEGER,  -- d
  prime1            INTEGER,  -- p
  prime2            INTEGER,  -- q
  exponent1         INTEGER,  -- d mod (p-1)
  exponent2         INTEGER,  -- d mod (q-1)
  coefficient       INTEGER,  -- (inverse of q) mod p
  otherPrimeInfos   OtherPrimeInfos OPTIONAL
}
*/

RSA.exportSync = function (opts) {
  if (!opts || !opts.jwk || 'object' !== typeof opts.jwk) {
    throw new Error("must pass { jwk: jwk }");
  }
  var jwk = JSON.parse(JSON.stringify(opts.jwk));
  var format = opts.format;
  var pub = opts.public;
  if (pub || -1 !== [ 'spki', 'pkix', 'ssh', 'rfc4716' ].indexOf(format)) {
    jwk = RSA.neuter(jwk);
  }
  if ('RSA' !== jwk.kty) {
    throw new Error("options.jwk.kty must be 'RSA' for RSA keys");
  }
  if (!jwk.p) {
    // TODO test for n and e
    pub = true;
    if (!format || 'pkcs1' === format) {
      format = 'pkcs1';
    } else if (-1 !== [ 'spki', 'pkix' ].indexOf(format)) {
      format = 'spki';
    } else if (-1 !== [ 'ssh', 'rfc4716' ].indexOf(format)) {
      format = 'ssh';
    } else {
      throw new Error("options.format must be 'spki', 'pkcs1', or 'ssh' for public RSA keys, not ("
        + typeof format + ") " + format);
    }
  } else {
    // TODO test for all necessary keys (d, p, q ...)
    if (!format || 'pkcs1' === format) {
      format = 'pkcs1';
    } else if ('pkcs8' !== format) {
      throw new Error("options.format must be 'pkcs1' or 'pkcs8' for private RSA keys");
    }
  }

  if ('pkcs1' === format) {
    if (jwk.d) {
      return PEM.packBlock({ type: "RSA PRIVATE KEY", bytes: x509.packPkcs1(jwk) });
    } else {
      return PEM.packBlock({ type: "RSA PUBLIC KEY", bytes: x509.packPkcs1(jwk) });
    }
  } else if ('pkcs8' === format) {
    return PEM.packBlock({ type: "PRIVATE KEY", bytes: x509.packPkcs8(jwk) });
  } else if (-1 !== [ 'spki', 'pkix' ].indexOf(format)) {
    return PEM.packBlock({ type: "PUBLIC KEY", bytes: x509.packSpki(jwk) });
  } else if (-1 !== [ 'ssh', 'rfc4716' ].indexOf(format)) {
    return SSH.pack({ jwk: jwk, comment: opts.comment });
  } else {
    throw new Error("Sanity Error: reached unreachable code block with format: " + format);
  }
};
RSA.pack = function (opts) {
  // wrapped in a promise for API compatibility
  // with the forthcoming browser version
  // (and potential future native node capability)
  return Promise.resolve().then(function () {
    return RSA.exportSync(opts);
  });
};
RSA.toPem = RSA.export = RSA.pack;

// snip the _private_ parts... hAHAHAHA!
var privates = [ 'p', 'q', 'd', 'dp', 'dq', 'qi' ];
// fix misspelling without breaking the API
RSA.neuter = RSA.nueter = function (priv) {
  var pub = {};
  Object.keys(priv).forEach(function (key) {
    if (!privates.includes(key)) {
      pub[key] = priv[key];
    }
  });
  return pub;
};

RSA.__thumbprint = function (jwk) {
  var buf = require('crypto').createHash('sha256')
    // alphabetically sorted keys [ 'e', 'kty', 'n' ]
    .update('{"e":"' + jwk.e + '","kty":"RSA","n":"' + jwk.n + '"}')
    .digest()
  ;
  return Enc.bufToUrlBase64(buf);
};

RSA.thumbprint = function (opts) {
  return Promise.resolve().then(function () {
    var jwk;
    if ('RSA' === opts.kty) {
      jwk = opts;
    } else if (opts.jwk) {
      jwk = opts.jwk;
    } else {
      jwk = RSA.importSync(opts);
    }
    return RSA.__thumbprint(jwk);
  });
};
